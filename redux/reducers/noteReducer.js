import {ADD_NOTE, setNotes, SET_NOTES} from '../actions/Note_Actions';
import {combineReducers} from 'redux';
const initialState = {
  notes: [],
};

const notesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NOTE: {
      return {...state, notes: [...state.notes, action.payload]};
    }
    case SET_NOTES: {
      return {...state, notes: action.payload};
    }
    default:
      return state;
  }
};

export const loadNotes = () => async (dispatch, getState) => {
  console.log('here we are');
  const notes = await fetch(
    'https://jsonplaceholder.typicode.com/todos/1',
  ).then((res) => res.json());
  console.log(notes);
  dispatch(setNotes(notes.title));
};
const reducers = combineReducers({
  loadNotes,
  notesReducer,
});
export default reducers;
