const ADD_NOTE = 'ADD_NOTE';
const SET_NOTES = 'SET_NOTES';

const addNote = (note) => ({
  type: ADD_NOTE,
  payload: note,
});

const setNotes = (notes) => ({
  type: SET_NOTES,
  payload: notes,
});

export {ADD_NOTE, SET_NOTES, addNote, setNotes};
