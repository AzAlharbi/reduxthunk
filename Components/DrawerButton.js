import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  StatusBar,
  TouchableOpacity,
} from 'react-native';

const Drawer = (props) => {
  return (
    <TouchableOpacity style={styles.drawer}>
      <View style={styles.first} />
      <View style={styles.second} />
      <View style={styles.third} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  drawer: {
    justifyContent: 'space-between',
    height: 26,
  },
  first: {
    backgroundColor: '#e6e6e6',
    height: 3,
    width: 20,
    borderRadius: 5,
  },
  second: {
    backgroundColor: '#e6e6e6',
    height: 3,
    width: 23,
    borderRadius: 5,
  },
  third: {
    backgroundColor: '#e6e6e6',
    height: 3,
    width: 26,
    borderRadius: 5,
  },
});

export default Drawer;
