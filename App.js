/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  StatusBar,
  TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {Provider} from 'react-redux';
import store from './redux/store';
import MainApp from './MainApp';

const App = () => {
  return (
    <>
      <Provider store={store}>
        <MainApp />
      </Provider>
    </>
  );
};

const HelloWorld = (props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.btn}>
        <Text style={styles.btnText}>{props.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    //backgroundColor: Colors.dark,
    backgroundColor: '#1a283a',
    height: '100%',
    padding: 15,
  },
  hello: {
    color: '#e6e6e6',
    fontWeight: 'bold',
    fontSize: 18,
  },
  awesomeSquare: {
    height: 150,
    borderRadius: 10,
    backgroundColor: '#d26f6f',
  },
  awesomeSquare2: {
    height: 150,
    borderRadius: 10,
    backgroundColor: '#6fb4d2',
  },
  awesomeSquare3: {
    height: 150,
    borderRadius: 10,
    backgroundColor: '#9dd26f',
  },
  header: {
    justifyContent: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
  body: {
    flex: 10,
    justifyContent: 'space-between',
  },
  btn: {
    borderRadius: 10,
    elevation: 5,
    shadowColor: 0xffcccccc,
    padding: 20,
    backgroundColor: '#00a8ff',
    alignItems: 'center',
  },
  btnText: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#ffffff',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },

  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
