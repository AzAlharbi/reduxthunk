import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import {ReloadInstructions, Colors} from 'react-native/Libraries/NewAppScreen';

const Component = () => {
  return (
    <View style={styles.sectionContainer}>
      <Text style={styles.sectionTitle}>See Your Changes</Text>
      <Text style={styles.sectionDescription}>
        <ReloadInstructions />
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
});

export default Component;
