import React from 'react';
import {View, Text, Button} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import AddNoteInput from './Components/AddNoteInput';
import {addNote} from './redux/actions/Note_Actions';
import {loadNotes} from './redux/reducers/noteReducer';

const MainApp = () => {
  //const notes = store.getState();
  const notes = useSelector((state) => state.notesReducer.notes);
  const dispatch = useDispatch();

  const onAddNote = (note) => {
    dispatch(addNote(note));
  };

  const onLoad = () => {
    dispatch(loadNotes());
  };
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <AddNoteInput addNote={onAddNote} />
      <View>
        {
          (notes.map((note) => {
            return <Text key={note}>{note}</Text>;
          }),
          console.log('Hello pop ' + notes))
        }
      </View>
      <Button title="Load" onPress={onLoad} />
    </View>
  );
};

export default MainApp;
