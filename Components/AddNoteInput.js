import React from 'react';
import {View, Text, Button, TextInput} from 'react-native';

const AddNoteInput = ({addNote}) => {
  const [note, setNote] = React.useState('hope');

  const onAddNoteClick = () => {
    addNote(note);
    setNote('');
  };

  return (
    <View>
      <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={(note) => setNote(note)}
        value={note}
        placeholder="Add note"
      />
      <Button title="save" onPress={onAddNoteClick} />
    </View>
  );
};

export default AddNoteInput;
